---
title: About
name: About
image: /images/me2.jpg
summary: About the author
---

# About me / links

I'm Saksham Sharma, a Computer-Scientist turned Quant, and Director of Quantitative Research Technology at [Tower Research Capital LLC](https://www.tower-research.com/), based out of New York. I lead our efforts on improving Machine Learning system performance for financial trading, while designing extremely low-latency trading systems and developing complex algorithmic trading strategies for global financial instruments that trade hundreds of millions of USD daily. Through my work, I help design, run, and scale Tower's quantitative trading strategies competitively across all the world's major financial exchanges, across all asset classes - improving market price-discovery and efficiency.

In addition to my work at Tower, I contribute to technical committees supporting the software required for high-performance financial systems. I am a member of [WG21, The ISO C++ committee](https://isocpp.org/std/the-committee) which helps standardize and improve the C++ programming language, and am a regular speaker at various major programming language conferences. I am also a member of the [Boost organization](https://www.boost.io/). I am involved in the C++ language community in various different capacities, am a regular at the [New York C++ meetup](https://www.meetup.com/new-york-c-c-meetup-group/), and am currently contributing to the development of the [Beman project](https://bemanproject.org/).

## Public Coverage
- [Coverage for my talk at PyCon 2024, Pittsburgh](https://thenewstack.io/why-python-is-so-slow-and-what-is-being-done-about-it/)
- President's Gold Medal
    * [Tweet by the President of India](https://x.com/rashtrapatibhvn/status/1012266336746123264)
    * [Tweet by the Director of IIT Kanpur](https://x.com/karandi65/status/1012249786836283392)
    * [News article covering this award](https://www.tribuneindia.com/news/archive/amritsar/city-boy-receives-gold-medal-from-president-612766)

## Tech talks
#### CppCon 2024 - Aurora, CO - Reflection
[CppCon](https://cppcon.org/) is the world's largest C++ language conference.\
[Link to talk page](https://cppcon2024.sched.com/event/1gZea/reflection-based-libraries-to-look-forward-to)
<iframe width="560" height="315" src="https://www.youtube.com/embed/7I40gHiLpiE?si=0EVo8XCo38frYkY7" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

#### PyCon 2024 - Pittsburgh, PA - Python and performance
[PyCon US](https://us.pycon.org/) is the world's largest Python language conference.\
[Link to talk page](https://us.pycon.org/2024/schedule/presentation/36/)
<iframe width="560" height="315" src="https://www.youtube.com/embed/4mEWoCB7oMY?si=Rte8cVNMMfa30DQe" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

#### CppCon 2023 - Aurora, CO - Python Bindings
[CppCon](https://cppcon.org/) is the world's largest C++ language conference.\
[Link to talk page](https://cppcon2023.sched.com/event/1QthL/writing-python-bindings-for-c-libraries-easy-to-use-performance)
<iframe width="560" height="315" src="https://www.youtube.com/embed/rB7c69Z5Kus?si=TEpzK-hMoPijl6Nd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

#### CppNow 2024 - Aspen, CO - Reflection in C++ and libraries
[CppNow](https://cppnow.org/) is an intimate conference hosted annually for C++ language experts.\
[Link to talk page](https://schedule.cppnow.org/session/2024/reflection-is-good-for-code-health/)
<iframe width="560" height="315" src="https://www.youtube.com/embed/GQ5HKL0WRGQ?si=R5syZOQGRCEXhwPj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

#### CppNow 2023 - Aspen, CO - Python Bindings
[CppNow](https://cppnow.org/) is an intimate conference hosted annually for C++ language experts.\
[Link to talk page](https://schedule.cppnow.org/session/2023/building-python-wrappers-for-c-libraries-easy-to-use-performance/)
<iframe width="560" height="315" src="https://www.youtube.com/embed/2rJJfTt72Dk?si=1b3VYaCPkR3S1nUM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

#### CppNorth - Toronto, Canada - Writing fast code like a native
[CppNorth](https://cppnorth.ca/) - The Canadian C++ Conference\
[Link to talk page](https://cppnorth2024.sched.com/event/1eQ7J/write-fast-code-like-a-native) - The talk was in July 2024, and is currently pending release on YouTube.

## Scholarly links
- [Google Scholar](https://scholar.google.co.in/citations?user=M1Nz-qsAAAAJ&hl=en)
- [GitHub](https://github.com/sakshamsharma/)
- C++ stuff
    * [P3095](https://wg21.link/P3095)
    * [Proposal for duck-typing with reflection](https://sakshamsharma.com/2024/05/duck/)